import { my_sum } from "./exercise-0.js";
import chai from "chai";

describe("Sum test", function () {
  it("check sum result", function () {
    chai.assert.equal(my_sum(2, 3), 5);
  });
  it("check that is equal to 0 when not numbers", function () {
    chai.assert.equal(my_sum("a", 2), 0);
  });
  it("check that is equal to 0 when parameter is null", function () {
    chai.assert.equal(my_sum(), 0);
  });
});
