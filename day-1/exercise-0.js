export const my_sum = (...args) => {
  if (args.length > 1) {
    if (typeof args[0] == "number" && typeof args[1] == "number") {
      return args[0] + args[1];
    }
  }
  return 0;
};
